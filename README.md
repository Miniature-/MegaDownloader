MegaDownloader
===============

### Introduction

PHP script to download files and folders (as ZIPs) directly to your webserver from Mega.nz

If you're downloading a folder, make sure you have enough local space available.

The default download path is the root path of the project.

Works on all webservers supporting PHP

Original version: https://github.com/ZonD80/mega-downloader/tree/standalone

In this version it's a lot easier to download files because you don't have to edit the PHP file every time you want to download a new file/folder, you just go to index.html and paste the link.


**Disclaimer:** I am not a programmer, this is just different stuff I've found on the internet and put together to make life easier for me. If Mega suddenly changes their encryption method or something along those lines then I won't be able to fix it.


### Installation

Duplicate the files to your webserver and go to index.html

### Screenshots
[index.html](https://minifiles.net/files/touvupl.png)

[execute.php](https://minifiles.net/files/ktyvdwz.png)

[index.html on mobile](https://minifiles.net/files/nvfncyy.png)